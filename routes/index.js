'use strict'

const refreshToken = require('../controllers/RefreshToken')
const verifyToken = require('../middleware/VerifyToken')

const UserController = require('../controllers/Users')
const CountryController = require('../controllers/Countries')
const LanguageController = require('../controllers/Languages')

module.exports = function (app) {
  // users
  app.get('/users', verifyToken, UserController.getUsers)
  app.post('/users', UserController.Register)
  app.post('/login', UserController.Login)
  app.delete('/logout', UserController.Logout)
  app.get('/token', refreshToken)

  // countries
  app.get(
    '/countries/:language_id',
    verifyToken,
    CountryController.getCountries
  )
  app.post('/countries', verifyToken, CountryController.createCountry)
  app.get(
    '/countries/:id/:language_id',
    verifyToken,
    CountryController.detailCountry
  )
  app.put('/countries/:id', verifyToken, CountryController.updateCountry)
  app.delete(
    '/countries/:id/:language_id',
    verifyToken,
    CountryController.deleteCountry
  )
  // languages
  app.get('/languages', verifyToken, LanguageController.getLanguages)
  app.post('/languages', verifyToken, LanguageController.createLanguage)
  app.get('/languages/:id', verifyToken, LanguageController.detailLanguage)
  app.put('/languages/:id', verifyToken, LanguageController.updateLanguage)
  app.delete('/languages/:id', verifyToken, LanguageController.deleteLanguage)
}
