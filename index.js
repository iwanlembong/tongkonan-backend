const dotenv = require('dotenv')
const express = require('express')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const path = require('path')
const cors = require('cors')
dotenv.config()
const { sequelize } = require('./db/models') // Import sequelize and models

const corsoptions = {
  origin: ['http://localhost:3000', 'http://staging.tongkonan.com'],
  methods: ['GET', 'POST', 'PUT', 'DELETE'], // Allow the required HTTP methods
  credentials: true,
  optionSuccessStatus: 200,
}
// const responseEnhancer = require('express-response-formatter')

const port = process.env.PORT || 5009

const app = express()

// sysnchronize
// try {
//   db.authenticate()
//   console.log('Database connected ....')
//   Language.sync()
// } catch (error) {
//   console.error(error)
// }
//  ens synchronie

/**
 * Expose
 */

module.exports = {
  app,
}

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(logger('dev'))
app.use(express.json())
app.use(cors(corsoptions))

app.use(cookieParser())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(express.static(path.join(__dirname, 'public')))
// app.use(responseEnhancer())

// Bootstrap routes
require('./routes/index.js')(app)

// Sync database and start server
sequelize
  .sync()
  .then(() => {
    console.log('Database & tables created!')

    app.listen(port, () => {
      console.log(`API app started on port ${port}`)
    })
  })
  .catch((err) => {
    console.error('Error creating database & tables:', err)
  })

// app.listen(port)

// console.log('API app started on port ' + port)
