const User = require('../db/models/User')
const { genSalt, hash, compare } = require('bcrypt')
const { sign } = require('jsonwebtoken')

class UserController {
  static async getUsers(req, res, next) {
    try {
      const user = await User.findAll({})
      res.status(200).json(user)
    } catch (error) {
      console.log(error)
    }
  }

  static async Register(req, res) {
    const { name, email, password, confPassword } = req.body
    if (password !== confPassword)
      return res
        .status(400)
        .json({ msg: 'Password and Confirmation Password is not match!' })

    const salt = await genSalt()
    const hashPassword = await hash(password, salt)

    try {
      await User.create({
        name: name,
        email: email,
        password: hashPassword,
      })

      res.json({ msg: 'Register user is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async Login(req, res) {
    try {
      const user = await User.findAll({
        where: {
          email: req.body.email,
        },
      })

      const match = await compare(req.body.password, user[0].password)

      if (!match) return res.status(400).json({ msg: 'Wrong password' })

      const userId = user[0].id
      const name = user[0].name
      const email = user[0].email

      const accessToken = sign(
        { userId, name, email },
        process.env.ACCESS_TOKEN_SECRET,
        {
          expiresIn: '1d',
        }
      )

      const refrehToken = sign(
        { userId, name, email },
        process.env.REFRESH_TOKEN_SECRET,
        {
          expiresIn: '1d',
        }
      )

      await User.update(
        { refresh_token: refrehToken },
        {
          where: {
            id: userId,
          },
        }
      )

      res.cookie('refreshToken', refrehToken, {
        httpOnly: true,
        maxAge: 24 * 60 * 60 * 1000, // umur 1 hari
        // secure: true, // kalau pakai https
      })

      res.json({ accessToken })
    } catch (error) {
      res.status(404).json({ msg: 'Email not found' })
    }
  }

  static async Logout(req, res) {
    const refreshToken = req.cookies.refreshToken

    if (!refreshToken) return res.sendStatus(204) // no content

    const user = await User.findAll({
      where: {
        refresh_token: refreshToken,
      },
    })

    if (!user[0]) return res.sendStatus(204) // no content

    const userId = user[0].id

    await User.update(
      { refresh_token: null },
      {
        where: {
          id: userId,
        },
      }
    )

    res.clearCookie('refreshToken')
    return res.sendStatus(200)
  }
}
module.exports = UserController
