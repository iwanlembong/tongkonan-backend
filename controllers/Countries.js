const Sequelize = require('sequelize')
// const Country = require('../db/models/Country')
// const CountryLang = require('../db/models/CountryLang')
// const Language = require('../db/models/Language')
const { Country, CountryLang, Language } = require('../db/models') // Adjust the path as needed

class CountryController {
  static async getCountries(req, res) {
    const langId = req.params.language_id
    try {
      const countries = await Country.findAll({
        include: [
          {
            model: CountryLang,
            attributes: ['country_name', 'capital_city'],
            as: 'countrylang',
            where: {
              language_id: langId,
            },
            include: [
              {
                model: Language,
                attributes: ['language_name'],
                as: 'language',
              },
            ],
          },
        ],
        order: [['country_code', 'ASC']],
      })

      // Mengonversi hasil menjadi format yang diinginkan
      const formattedCountries = countries.map((country) => ({
        id: country.id,
        country_code: country.country_code,
        iso_code1: country.iso_code1,
        iso_code2: country.iso_code2,
        country_name: country.countrylang[0]?.country_name || '',
        capital_city: country.countrylang[0]?.capital_city || '',
        language: country.countrylang[0]?.language.language_name || '',
      }))

      res.status(200).json(formattedCountries)
    } catch (error) {
      console.log(error)
    }
  }

  static async createCountry(req, res) {
    const {
      county_code,
      country_name,
      capital_city,
      iso_code1,
      iso_code2,
      language_id,
      username,
    } = req.body
    //cek apakah country sdh ada
    const countries = await CountryLang.findAll({
      where: {
        country_name: country_name,
      },
    })

    if (countries[0])
      return res.status(400).json({ msg: 'Country is alredy exist' })

    try {
      // Create new country
      const newCountry = await Country.create({
        country_code: county_code,
        continent_id: 0,
        iso_code1: iso_code1,
        iso_code2: iso_code2,
        createdAt: Sequelize.fn('NOW'),
        createdBy: username,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: username,
      })

      await CountryLang.create({
        country_id: newCountry.id, // Use the ID of the newly created country
        country_name: country_name,
        capital_city: capital_city,
        language_id: language_id,
        createdAt: Sequelize.fn('NOW'),
        createdBy: username,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: username,
      })

      res.json({ msg: 'Add new Country successfully' })
    } catch (error) {
      console.log(error)
      res
        .status(500)
        .json({ msg: 'An error occurred while creating the country' })
    }
  }

  static async detailCountry(req, res) {
    const langId = req.params.language_id
    const countryId = req.params.id
    try {
      const countries = await Country.findAll({
        include: [
          {
            model: CountryLang,
            attributes: ['country_name', 'capital_city'],
            as: 'countrylang',
            where: {
              language_id: langId,
            },
            include: [
              {
                model: Language,
                attributes: ['language_name'],
                as: 'language',
              },
            ],
          },
        ],
        where: {
          id: countryId,
        },
      })

      // Mengonversi hasil menjadi format yang diinginkan
      const formattedCountry = countries.map((country) => ({
        id: country.id,
        country_code: country.country_code,
        iso_code1: country.iso_code1,
        iso_code2: country.iso_code2,
        country_name: country.countrylang[0]?.country_name || '',
        capital_city: country.countrylang[0]?.capital_city || '',
        language: country.countrylang[0]?.language.language_name || '',
      }))

      res.status(200).json(formattedCountry)
    } catch (error) {
      console.log(error)
    }
  }

  static async updateCountry(req, res) {
    const countryId = req.params.id
    const {
      county_code,
      country_name,
      capital_city,
      iso_code1,
      iso_code2,
      language_id,
      username,
    } = req.body
    //cek apakah language sdh ada
    const countries = await Country.findAll({
      where: {
        id: countryId,
      },
    })

    if (!countries[0])
      return res.status(400).json({ msg: 'Country is not exist' })

    try {
      await Country.update(
        {
          county_code: county_code,
          iso_code1: iso_code1,
          iso_code2: iso_code2,
          updatedBy: username,
          updatedAt: Sequelize.fn('NOW'),
        },
        {
          where: {
            id: countryId,
          },
        }
      )

      // Update the CountryLang table
      await CountryLang.update(
        {
          country_name: country_name,
          capital_city: capital_city,
          updatedBy: username,
          updatedAt: Sequelize.fn('NOW'),
        },
        {
          where: {
            country_id: countryId,
            language_id: language_id,
          },
        }
      )

      res.json({ msg: 'Update Country is successfully' })
    } catch (error) {
      console.log(error)
      res
        .status(500)
        .json({ msg: 'An error occurred while updating the country' })
    }
  }

  static async deleteCountry(req, res) {
    const countryId = req.params.id
    const langId = req.params.language_id
    //cek apakah language sdh ada
    //cek apakah language sdh ada
    const countries = await CountryLang.findAll({
      where: {
        country_id: countryId,
        language_id: langId,
      },
    })

    if (!countries[0])
      return res.status(400).json({ msg: 'Country is not exist' })

    try {
      await Language.destroy({
        where: {
          id: countryId,
        },
      })

      await CountryLang.destroy({
        where: {
          country_id: countryId,
          language_id: langId,
        },
      })

      res.json({ msg: 'Delete Country is successfully' })
    } catch (error) {
      console.log(error)
    }
  }
}

module.exports = CountryController
