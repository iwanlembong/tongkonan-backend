const Sequelize = require('sequelize')
const Language = require('../db/models/Language')

class LanguageController {
  static async getLanguages(req, res) {
    try {
      const languages = await Language.findAll({})
      res.status(200).json(languages)
    } catch (error) {
      console.log(error)
    }
  }

  static async createLanguage(req, res) {
    const { language_code, language_name, username } = req.body
    try {
      await Language.create({
        language_code: language_code,
        language_name: language_name,
        createdAt: Sequelize.fn('NOW'),
        createdBy: username,
        updatedAt: Sequelize.fn('NOW'),
        updatedBy: username,
      })

      res.json({ msg: 'Create language is successfully' })
    } catch (error) {
      console.log(error)
    }
  }

  static async detailLanguage(req, res) {
    const langId = req.params.id
    try {
      const language = await Language.findOne({
        where: {
          id: langId,
        },
      })
      res.status(200).json(language)
    } catch (error) {
      console.log(error)
    }
  }

  static async updateLanguage(req, res) {
    const langId = req.params.id
    const { language_code, language_name, username } = req.body
    //cek apakah language sdh ada
    const languages = await Language.findAll({
      where: {
        id: langId,
      },
    })

    if (!languages[0])
      return res.status(400).json({ msg: 'Language is not exist' })

    try {
      await Language.update(
        {
          language_code: language_code,
          language_name: language_name,
          updatedBy: username,
          updatedAt: Sequelize.fn('NOW'),
        },
        {
          where: {
            id: langId,
          },
        }
      )

      res.json({ msg: 'Update Language is successfully' })
    } catch (error) {
      console.log(error)
      res
        .status(500)
        .json({ msg: 'An error occurred while updating the country' })
    }
  }

  static async deleteLanguage(req, res) {
    const langId = req.params.id

    //cek apakah language sdh ada
    const languages = await Language.findAll({
      where: {
        id: langId,
      },
    })

    if (!languages[0])
      return res.status(400).json({ msg: 'Language is not exist' })

    try {
      await Language.destroy({
        where: {
          id: langId,
        },
      })

      res.json({ msg: 'Delete Language is successfully' })
    } catch (error) {
      console.log(error)
    }
  }
}

module.exports = LanguageController
