import Products from '../db/models/ProductModel.js'
import Sequelize from 'sequelize'

export const getProducts = async (req, res) => {
  try {
    const products = await Products.findAll({
      attributes: ['id', 'product_name', 'price'],
    })
    res.json(products)
  } catch (error) {
    console.log(error)
  }
}

export const createProduct = async (req, res) => {
  const { product_name, price } = req.body
  try {
    await Products.create({
      product_name: product_name,
      price: price,
      createdAt: Sequelize.fn('NOW'),
      updatedAt: Sequelize.fn('NOW'),
    })

    res.json({ msg: 'Create products is successfully' })
  } catch (error) {
    console.log(error)
  }
}

export const getProductById = async (req, res) => {
  const productId = req.params.id
  try {
    const product = await Products.findOne({
      where: {
        id: productId,
      },
    })
    res.status(200).json(product)
  } catch (error) {
    console.log(error)
  }
}

export const updateProduct = async (req, res) => {
  const productId = req.params.id
  const { product_name, price } = req.body
  //cek apakah Menu sdh ada
  const products = await Products.findAll({
    where: {
      id: productId,
    },
  })

  if (!products[0])
    return res.status(400).json({ status: 400, msg: 'Product is not exist' })

  try {
    await Products.update(
      {
        product_name: product_name,
        price: price,
        updatedAt: Sequelize.fn('NOW'),
      },
      {
        where: {
          id: productId,
        },
      }
    )

    res.json({ status: 200, msg: 'Update Product is successfully' })
  } catch (error) {
    console.log(error)
  }
}

export const deleteProduct = async (req, res) => {
  const productId = req.params.id
  //cek apakah Menu sdh ada
  const products = await Products.findAll({
    where: {
      id: productId,
    },
  })

  if (!products[0])
    return res.status(400).json({ status: 400, msg: 'Product is not exist' })

  try {
    await Products.destroy({
      where: {
        id: productId,
      },
    })

    res.json({ status: 200, msg: 'Delete Product is successfully' })
  } catch (error) {
    console.log(error)
  }
}
