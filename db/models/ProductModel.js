import { Sequelize } from 'sequelize'
import db from '../config/database.js'

const { DataTypes } = Sequelize

const Products = db.define(
  'products',
  {
    product_name: {
      type: DataTypes.STRING,
    },
    price: {
      type: DataTypes.DECIMAL,
    },
  },
  {
    freezeTableName: true,
  }
)

export default Products
