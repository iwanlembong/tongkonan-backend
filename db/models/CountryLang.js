'use strict'

const { Model, DataTypes } = require('sequelize')
const sequelize = require('../../config/database') // path to your database config

class CountryLang extends Model {
  static associate(models) {
    CountryLang.belongsTo(models.Country, {
      foreignKey: 'country_id',
      as: 'country',
    })

    CountryLang.belongsTo(models.Language, {
      foreignKey: 'language_id',
      as: 'language',
    })
  }
}

CountryLang.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    country_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Country',
        key: 'id',
      },
    },
    country_name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Country name is required.',
        },
        notEmpty: {
          msg: 'Country name is required',
        },
      },
    },
    capital_city: DataTypes.STRING,
    language_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    is_deleted: DataTypes.INTEGER,
    createdBy: {
      type: DataTypes.STRING,
    },
    updatedBy: {
      type: DataTypes.STRING,
    },
  },
  {
    sequelize,
    modelName: 'CountryLang',
    tableName: 'countries_lang',
  }
)

module.exports = CountryLang
