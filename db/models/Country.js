'use strict'

const { Model, DataTypes } = require('sequelize')
const sequelize = require('../../config/database') // path to your database config

class Country extends Model {
  static associate(models) {
    Country.hasMany(models.CountryLang, {
      as: 'countrylang',
      foreignKey: 'country_id',
    })
  }
}

Country.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    country_code: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Country code is required.',
        },
        notEmpty: {
          msg: 'Country code is required',
        },
      },
    },
    continent_id: {
      type: DataTypes.INTEGER,
    },
    iso_code1: {
      type: DataTypes.STRING,
    },
    iso_code2: {
      type: DataTypes.STRING,
    },
    is_deleted: {
      type: DataTypes.INTEGER,
    },
    createdBy: {
      type: DataTypes.STRING,
    },
    updatedBy: {
      type: DataTypes.STRING,
    },
  },
  {
    sequelize,
    modelName: 'Country',
    tableName: 'countries',
  }
)

module.exports = Country
