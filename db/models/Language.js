'use strict'

const { Model, DataTypes } = require('sequelize')
const sequelize = require('../../config/database') // path to your database config

class Language extends Model {
  static associate(models) {
    Language.hasMany(models.CountryLang, {
      foreignKey: 'language_id',
      as: 'countrylang',
    })
  }
}

Language.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    language_code: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    language_name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    is_deleted: {
      type: DataTypes.INTEGER,
    },
    createdBy: {
      type: DataTypes.STRING,
    },
    updatedBy: {
      type: DataTypes.STRING,
    },
  },
  {
    sequelize,
    modelName: 'Language',
    tableName: 'languages', // make sure this matches your actual table name
  }
)

module.exports = Language
