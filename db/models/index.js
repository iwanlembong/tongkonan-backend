'use strict'

// models/index.js
const sequelize = require('../../config/database') // Adjust the path as needed
const Country = require('./Country') // Adjust the path as needed
const CountryLang = require('./CountryLang') // Adjust the path as needed
const Language = require('./Language') // Adjust the path as needed

const models = {
  Country,
  CountryLang,
  Language,
}

// Call associate methods
Object.keys(models).forEach((modelName) => {
  if (models[modelName].associate) {
    models[modelName].associate(models)
  }
})

module.exports = {
  sequelize,
  ...models,
}
